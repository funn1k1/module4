﻿using System;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
           //Not tested
        }


        public int Task_1_A(int[] array)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            else if (array.Length == 0) throw new ArgumentNullException(nameof(array));
            int max = array[0];
            for(int i = 0; i < array.Length; i++)
            {
                if (max < array[i])
                {
                    max = array[i];
                }
            }
            return max;
        }

        public int Task_1_B(int[] array)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            else if (array.Length == 0) throw new ArgumentNullException(nameof(array));
            int min = array[0];
            for(int i = 0; i < array.Length; i++)
            {
                if (min > array[i])
                {
                    min = array[i];
                }
            }
            return min;
        }

        public int Task_1_C(int[] array)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            else if (array.Length == 0) throw new ArgumentNullException(nameof(array));
            int sum = 0;
            for(int i = 0; i < array.Length; i++)
            {
                sum += array[i];
            }
            return sum;
        }

        public int Task_1_D(int[] array)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            else if(array.Length==0) throw new ArgumentNullException(nameof(array));
            int max = array[0], min = array[0];
            for(int i = 0; i < array.Length; i++)
            {
                if (array[i] > max) max = array[i];
                else if (array[i] < min) min = array[i];
            }
            return max - min;
        }

        public void Task_1_E(int[] array)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            else if (array.Length == 0) throw new ArgumentNullException(nameof(array));
            int max = Task_1_A(array);
            int min = Task_1_B(array);
            for(int i=0;i<array.Length;i++)
            {
                if (i%2==0) array[i] += max;
                else if(i%2!=0) array[i] -= min;
            }
        }

        public int Task_2(int a, int b, int c)
        {
            return a + b + c;
        }

        public int Task_2(int a, int b)
        {
            return a + b;
        }

        public double Task_2(double a, double b, double c)
        {
            return a + b + c;
        }

        public string Task_2(string a, string b)
        {
            return a + b;
        }

        public int[] Task_2(int[] a, int[] b)
        {
            int[] array1  = a.Length < b.Length ? b : a;
            int[] array2 = a.Length < b.Length ? a : b;
            for (int i = 0; i < array2.Length; i++) array1[i] += array2[i];
            return array1;
        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10; b += 10; c += 10;
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            if (radius < 0) throw new ArgumentException(nameof(radius));
            length = 2 * Math.PI * radius;
            square = Math.PI * Math.Pow(radius, 2);
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            maxItem = array[0]; minItem = array[0];sumOfItems = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > maxItem) maxItem = array[i];
                else if (array[i] < minItem) minItem = array[i];
                sumOfItems += array[i];
            }
        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            var get = (numbers.Item1 + 10, numbers.Item2 + 10, numbers.Item3 + 10);
            return get;
        }

        public (double, double) Task_4_B(double radius)
        {
            if (radius < 0) throw new ArgumentException(nameof(radius));
            return ((2 * Math.PI * radius), (Math.PI * Math.Pow(radius, 2)));
        }

        public (int, int, int) Task_4_C(int[] array)
        {
            Task_3_C(array, out int maxItem, out int minItem, out int sumOfItems);
            return (minItem, maxItem, sumOfItems);
        }

        public void Task_5(int[] array)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            else if (array.Length == 0) throw new ArgumentNullException(nameof(array));
            for (int i = 0; i < array.Length; i++) array[i] += 5;
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            if (array.Length == 0) throw new ArgumentNullException(nameof(array));
            if (direction == SortDirection.Ascending)
            {
                Array.Sort(array);
            }
            else if (direction == SortDirection.Descending)
            {
                Array.Sort(array);
                Array.Reverse(array);
            }
        }        

        public  double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            if (Math.Abs(x1 - x2) / Math.Abs(x2) >= e)
            {
                double x = (x1 + x2) / 2;
                if (func(x) * func(x1) < 0) x2 = x;
                else x1 = x;
                return Task_7(func, x1, x2, e, result);
            }
            else return (x1 + x2) / 2;
        }
    }
}
